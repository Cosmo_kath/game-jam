using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject SoulPrefab, KeyPrefab;
    private GameManager gm;
    public float speed;
    private Transform target;
    public float life;

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
    }

    public void takedamage()
    {
        life -= gm.strength;

        if (life >= 0) ondeath();
    }

    //We need to call this right BEFORE killing the monster.
    void ondeath()
    {
        int number = Random.Range(1, 11);
        gm.Count();

        if (number == 1)
        {
            GameObject soul = Instantiate(SoulPrefab, transform.position, transform.rotation) as GameObject;
        }
        if (gm.enemyCount == 1)
        {
            GameObject Key = Instantiate(KeyPrefab, transform.position, transform.rotation) as GameObject;
        }

        Destroy(gameObject);
        
    }
}
