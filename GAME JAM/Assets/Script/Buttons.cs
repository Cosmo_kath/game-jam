using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
    public GameObject creditPanel;

    public void ShowCredits()
    {
        creditPanel.SetActive(true);
    }
    public void hideCredits()
    {
        creditPanel.SetActive(false);
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
