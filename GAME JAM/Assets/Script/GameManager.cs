﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public float speed = 1, attackDelay = 1, strength = 1, bulletSpeed = 10, lastFire = 0;

    public float enemyCount = 0;

    public int health, maxHearts;
    public Image[] hearts;
    public Sprite fullheart, emptyheart;

    public GameObject[] enemys;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Update()
    {
        if (health > maxHearts)
        {
            health = maxHearts;
        }
        for (int i = 0; i < hearts.Length; i++)
        {
            if (i < health)
            {
                hearts[i].sprite = fullheart;
            }
            else
            {
                hearts[i].sprite = emptyheart;
            }

            if (i < maxHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }

    void Start()
    {
        Count();
    }

    public void Count()
    {
        enemyCount = GameObject.FindGameObjectsWithTag("Enemy").Length;

        Debug.Log("There is " + enemyCount + " enemy(s) left");
    }
}
