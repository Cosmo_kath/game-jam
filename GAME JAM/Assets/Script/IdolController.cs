using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdolController : MonoBehaviour
{
    private bool isClose = false;
    private GameManager gm;
    private GameObject tinkersbellpowder;
    private bool used = false;
    void Start()
    {
        tinkersbellpowder = GameObject.Find("tinkerbellsCocaine");
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
    }

    void Update()
    {
        if (isClose == true && used == false)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                randomEffect();
                used = true;
            }
        }
    }
    void OnTriggerStay2D(Collider2D collision)
    {
        isClose = true;
    }
        
    void randomEffect()
    {
        int number = Random.Range(1, 39);
        Debug.Log("number picked : " + number);
        switch (number)
        {
            //Must add items in gamemanager before completing this.
            case int n when (number <= 5): //health + (1 à 5)
                gm.maxHearts += 1;
                break;
            case int n when (number < 11): //Speed + (6 à 10)
                gm.speed += 2;
                break;
            case int n when (number < 16): //AttackRate + (11 à 15)
                if (gm.attackDelay > 0.2)
                {
                    gm.attackDelay -= (float)0.2;
                }
                else randomEffect();
                break;
            case int n when (number < 21): //Strength + (16 à 20)
                gm.strength += 1;
                break;
            case int n when (number < 26): //bulletspeed + (21 à 25)
                gm.bulletSpeed += 1;
                break;
            case int n when (number < 28): //health - (26 à 28)
                if (gm.maxHearts > 1)
                {
                    gm.maxHearts -= 1;
                }
                else randomEffect();
                break;
            case int n when (number < 30): //Speed - (28 à 30)
                if (gm.speed > 1)
                {
                    gm.speed -= 1;
                }
                else randomEffect();
                break;
            case int n when (number < 32): //AttackRate - (30 à 32)
                gm.attackDelay += (float)0.2;
                break;
            case int n when (number < 34): //Strength - (32 à 34)
                if (gm.strength > 1)
                {
                    gm.strength -= 1;
                }
                else randomEffect();
                break;
            case int n when (number < 36): //bulletspeed - (36 à 38)
                if (gm.strength > 1)
                {
                    gm.strength -= 1;
                }
                else randomEffect();
                gm.bulletSpeed -= 1;
                break;
        }
        tinkersbellpowder.SetActive(false);
    }
}
