using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Vector2 moveVelocity;
    private Rigidbody2D body;
    public GameObject BulletPrefab;

    private GameManager gm;

    [SerializeField]
    private float speed;

    [SerializeField]
    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 moveInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        moveVelocity = moveInput.normalized * gm.speed;

        float shootHor = Input.GetAxis("ShootHorizontal");
        float shootVer = Input.GetAxis("ShootVertical");
        //Vector2 shootInput = new Vector2(Input.GetAxis("ShootHorizontal"), Input.GetAxis("ShootVertical"));
        if ((shootHor != 0 || shootVer != 0) && Time.time > gm.lastFire + gm.attackDelay)
        {
            pewpew(shootHor, shootVer);
            gm.lastFire = Time.time;
        }

        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 currentVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;

        float newVelocityX = 0f;
        if (moveHorizontal < 0 && currentVelocity.x <= 0)
        {
            newVelocityX = -speed;
            animator.SetInteger("DirectionX", -1);
        }
        else if (moveHorizontal > 0 && currentVelocity.x >= 0)
        {
            newVelocityX = speed;
            animator.SetInteger("DirectionX", 1);
        }
        else
        {
            animator.SetInteger("DirectionX", 0);
        }

        float newVelocityY = 0f;
        if (moveVertical < 0 && currentVelocity.y <= 0)
        {
            newVelocityY = -speed;
            animator.SetInteger("DirectionY", -1);
        }
        else if (moveVertical > 0 && currentVelocity.y >= 0)
        {
            newVelocityY = speed;
            animator.SetInteger("DirectionY", 1);
        }
        else
        {
            animator.SetInteger("DirectionY", 0);
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(newVelocityX, newVelocityY);

    }

    void FixedUpdate()
    {
        body.MovePosition(body.position + moveVelocity * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            gm.health -= 1;
            Debug.Log("Damaged");
        }
        if (collision.gameObject.tag == "Soul")
        {
            gm.health += 1;
            Destroy(collision.gameObject);
            Debug.Log("Healed");
        }
        if (collision.gameObject.tag == "Key")
        {
            Destroy(collision.gameObject);
        }
    }

    private void pewpew(float x, float y)
    {
        GameObject bullet = Instantiate(BulletPrefab, GameObject.Find("pewpewpoint").transform.position, transform.rotation) as GameObject;
        bullet.AddComponent<Rigidbody2D>().gravityScale = 0;
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector3(
            (x < 0) ? Mathf.Floor(x) * gm.bulletSpeed : Mathf.Ceil(x) * gm.bulletSpeed,
            (y < 0) ? Mathf.Floor(y) * gm.bulletSpeed : Mathf.Ceil(y) * gm.bulletSpeed,
            0
            );
    }
}
